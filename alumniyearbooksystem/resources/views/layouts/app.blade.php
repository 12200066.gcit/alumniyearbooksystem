<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
     
        <x-sidebar/>
<main>
    {{ $slot }}
    </main> 
</div>

<!-- </div>   -->
    </body>
    <script>
        
function toggleSubmenu(submenuId) {
    var submenu = document.getElementById(submenuId);
    if (submenu.style.display === "block") {
        submenu.style.display = "none";
    } else {
        submenu.style.display = "block";
    }
}
function togglePasswordVisibility() {
    var passwordField = document.getElementById("password");
    if (passwordField.type === "password") {
        passwordField.type = "text";
    } else {
        passwordField.type = "password";
    }
}
function displayImage(event) {
    const fileInput = event.target;
    const uploadedImage = document.getElementById("uploadedImage");

    // Check if a file was selected
    if (fileInput.files.length > 0) {
        const file = fileInput.files[0];
        const imageURL = URL.createObjectURL(file);
        uploadedImage.src = imageURL;
        uploadedImage.style.display = "block"; // Show the image
    } else {
        uploadedImage.src = ""; // Clear the image source
        uploadedImage.style.display = "none"; // Hide the image
    }
}
function editRow(rowNumber) {
    // Implement your edit logic here for the row with the specified rowNumber.
    // You can access the data in that row using the rowNumber.
    alert("Editing row " + rowNumber);
}

function deleteRow(rowNumber) {
    // Implement your delete logic here for the row with the specified rowNumber.
    // You can access the data in that row using the rowNumber.
    alert("Deleting row " + rowNumber);
}
function confirmDelete(rowId) {
    var result = confirm("Are you sure you want to delete this item?");
    if (result) {
        // User clicked "Yes," perform the deletion
        deleteRow(rowId);
    }
}
    </script>
</html>
