<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<style>

        /* Style for the main cards container */
        .main-cards {
            display: flex;
            justify-content: center;
            margin-top: 10%;
            margin-left:30%;
            /* border: 1px red solid; */
            position: relative;
            top:5%;
            left: 50%;

        }

        /* Style for individual cards */
        .card {
            background-color: #ccc; /* Change main card background color to gray */
            border: 1px solid #ccc;
            border-radius: 5px;
            padding: 10px;
            width: 60%; 
            height: 20%;/* Set a fixed width for the cards */ /* Add more margin for increased spacing between cards */
            display: flex;
            flex-direction: column; /* Display card contents vertically */
            align-items: center; /* Center align card contents */
            position: relative; 
            margin: 40px;
        
        }

        /* Style for the green line separator */
        .separator {
            width: 100%;
            height: 2px;
            background-color: #01605A; /* Green color */
            margin: 10px 0; /* Add spacing above and below the line */
        }

        /* Style for the first part of the card */
        .card-top {
            text-align: center;
        }

        .card-top h3 {
            font-size: 18px;
            font-weight: bold;
            margin-bottom: 10px;
        }

        /* Style for the second part of the card */
        .card-bottom {
            text-align: center;
            color: #777;
            height: 50%;
        }

        /* Style for the small card */
        .small-card {
            position: absolute; /* Position it absolutely within the main card */
            top: -20px; /* Adjust the distance from the top of the main card */
            left: 10px;
            height:60px; /* Adjust the distance from the left side */
            width: 60px; /* Adjust the width of the small card */
            background-color: #01605A; /* Orange color for small card */
            border: 1px solid #fff; /* White border for small card */
            border-radius: 5px;
            padding: 5px;
            text-align: center;
            z-index: 1; /* Ensure it's on top of the main card */
        }
        .number{
            margin-left: 200px;
        }
        .top{
            margin-top: 20px;
            text-align:center;
            justify-items: center;

        }

  .header{
    width: 80%;
    height: 10%;
    position: fixed;
    top: 0;
    left: 20%;
    background-color:#00605a;
    border-bottom: 2px solid #709BD4;
     
  }
 
</style>
<body>
<x-sidebar/>
<div class="main-container">
    <div class="main-cards">
        <div class="card">
            <div class="small-card">
                <p class ="top">CS</p>
            </div>
            <div class="card-top">
                <h3>Graduated</h3>
                <h1 class="number">{{$totalCS}}</h1>
            </div>
            <div class="separator"></div> <!-- Green line separator -->
            <div class="card-bottom">
                <span class="all">Computer science</span>
            </div>
        </div>
        <div class="card">
            <div class="small-card">
                <p class ="top">IT</p>
            </div>
            <div class="card-top">
                <h3>Graduated</h3>
                <h1 class="number">{{$totalIT}}</h1>
            </div>
            <div class="separator"></div> <!-- Green line separator -->
            <div class="card-bottom">
                <span class=" all">Information technology</span>
            </div>
        </div>
    </div>
   
    </div>
    <div style="height: 50%; width:70%; position:fixed; top:40%; left:25%"> <canvas  id="bookingsChart" width="100%"></canvas></div>
   
    </div>
        
    </body>
    <script>
  document.addEventListener('DOMContentLoaded', function () {
    fetch('/studentcard/chart')
        .then(response => response.json())
        .then(data => {
            console.log('Chart Data:', data); // Debugging statement
            var ctx = document.getElementById('bookingsChart').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: data.labels,
                    datasets: [{
                        label: 'No. students',
                        data: data.counts,
                        backgroundColor: 'rgba(75, 192, 192, 0.2)',
                        borderColor: 'rgba(75, 192, 192, 1)',
                        borderWidth: 1
                    }]
                },
                options: {
                    maintainAspectRatio: false, // Set to false to allow custom width and height
                    responsive: true,
                    width: 50, // Set your desired width
                    height: 200,// Make the chart responsive
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
        })
        .catch(error => console.error('Error fetching chart data:', error)); // Debugging statement
});

    </script>
