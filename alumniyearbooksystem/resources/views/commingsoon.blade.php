<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Coming Soon</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            text-align: center;
            margin: 0;
            padding: 0;
        }
        .container {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }
        .content {
            max-width: 600px;
            margin: 0 auto;
        }
        h1 {
            font-size: 36px;
        }
        p {
            font-size: 18px;
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="content">
            <h1>Coming Soon!</h1>
            <p>Our exciting new  yearbook.</p>
            <!-- You can add social media icons/links here -->
        </div>
    </div>
</body>
</html>
