<style>
    .custom-margin{
        margin-top: 35px;
        margin-left: 90%;
        height: auto;
        overflow-y: scroll;
        width: 100%;
    }
    .main-container {
    display: flex;
}

.child-container {
    flex: 1;
    padding: 20px;
    border: 1px solid #ccc;
    margin: 10px;
}
</style>
<x-app-layout>

<div>
    
</div>
    <div class="custom-margin">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6">
            <div class="p-2 sm:p-2 shadow sm:rounded-lg">
                <div class="max-w-xl">
                    @include('profile.partials.update-profile-information-form')
                </div>
            </div>

            <div class="p-2 sm:p-2 shadow sm:rounded-lg">
                <div class="max-w-xl">
                    @include('profile.partials.update-password-form')
                </div>
            </div>
            <!-- <div class="p-1 sm:p-1 bg-white dark:bg-gray-800 shadow sm:rounded-lg">
                <div class="max-w-xl">
                    @include('profile.partials.delete-user-form')
                </div>
            </div> -->
        </div>
    </div>
</x-app-layout>
