<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
   <script src="{{ asset('build/assets/js/extras/jquery.min.1.7.js') }}"></script>
    <script src="{{ asset('build/assets/js/extras/jquery-ui-1.8.20.custom.min.js') }}"></script>
    <script src="{{ asset('build/assets/js/extras/jquery.mousewheel.min.js') }}"></script>
    <script src="{{ asset('build/assets/js/extras/modernizr.2.5.3.min.js') }}"></script>
    <script src="{{ asset('build/assets/js/lib/hash.js') }}"></script>

 </head>
<style>
.download {
  background-color: #FFFFFF;
  border: 1px solid rgb(209,213,219);
  border-radius: .5rem;
  position: fixed;
  top: 2px;
  right: 10px;
  color: #111827;
  font-family: ui-sans-serif,system-ui,-apple-system,system-ui,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
  font-size: .875rem;
  font-weight: 600;
  line-height: 1.25rem;
  padding: .75rem 1rem;
  text-align: center;
  -webkit-box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.05);
  box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.05);
  cursor: pointer;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-user-select: none;
  -ms-touch-action: manipulation;
  touch-action: manipulation;
}

.download:hover {
  background-color: green;
}

.download:focus {
  outline: 2px solid rgba(0,0,0,0.1);
  outline-offset: 2px;
}

.download:focus-visible {
  -webkit-box-shadow: none;
  box-shadow: none;
}

button:hover {
  opacity:1;
}
.clearfix{
    display: flex;
    flex-direction: row;
}
.clear{
    flex:1;
    width:50%;
    padding: 20px;
}
/* Float cancel and delete buttons and add an equal width */
.cancelbtn, .deletebtn {
  float: left;
  width: 50%;
  background-color: #04AA6D;
  color: white;
  padding: 14px 20px;
  margin: 8px ;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
  /* width: 100%; */
  border-radius: 10%;
}

/* Add a color to the cancel button */
.cancelbtn {
  background-color: #ccc;
  color: black;
}

/* Add a color to the delete button */
a{
    text-decoration: none;
    color: black;
}

/* Add padding and center-align text to the container */
.container {
  /* padding: 16px; */
  text-align: center;
}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index:1000;; 
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: #474e5d;
  padding-top: 50px;
}

/* Modal Content/Box */
.modal-content {
  background-color: #fefefe;
  margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
  border: 1px solid #888;
  width: 80%; /* Could be more or less, depending on screen size */

}

/* Style the horizontal ruler */
hr {
  border: 1px solid #f1f1f1;
  margin-bottom: 25px;
}

/* The Modal Close Button (x) */
.close {
  position: absolute;
  right: 35px;
  top: 15px;
  font-size: 40px;
  font-weight: bold;
  color: #f1f1f1;
}

.close:hover,
.close:focus {
  color: #f44336;
  cursor: pointer;
}

/* Clear floats */
.clearfix::after {
  content: "";
  clear: both;
  display: table;
}

</style>
<body>
<button onclick="document.getElementById('id01').style.display='block'" class="download">Download Yearbook</button>
    <div id="id01" class="modal">
  <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
  <form class="modal-content" action="/action_page.php">
    <div class="container">
      <h1>Download</h1>
      <p>Are you sure you want to download the yearbook?</p>

      <div class="clearfix">
        <div class="clear"><button type="button" class="deletebtn"> <a href="{{route('generate-pdf')}}">Yes</a></button></div>
        <div class="clear"><button type="button" class="cancelbtn">cancel</button></div>
   
       
       
      </div>
    </div>
  </form>
</div>
    @if(Route::currentRouteName() === 'generate-pdf')
                <img src="{{ public_path('build/assets/pics/cover20-removebg-preview.png') }}" style="width:100%" />
            @endif
    <div id="canvas">
        <div id="book-zoom">
            <div class="sj-book">
                <div depth="5" class="hard"> <div class="side"></div> </div>
                <div depth="5" class="hard front-side"> <div class="depth"></div> </div>
                <div class="own-size"></div>
                <div class="own-size even"></div>
                <div class="hard fixed back-side p10"> <div class="depth"></div> </div>
                <div class="hard p12"></div>
            </div>
        </div>
        <div id="slider-bar" class="turnjs-slider">
            <div id="slider"></div>
        </div>
    </div>
    

<script type="text/javascript">

    function loadApp() {
        
        var flipbook = $('.sj-book');
    
        // Check if the CSS was already loaded
        
        if (flipbook.width()==0 || flipbook.height()==0) {
            setTimeout(loadApp, 10);
            return;
        }
    
        // Mousewheel
    
        $('#book-zoom').mousewheel(function(event, delta, deltaX, deltaY) {
    
            var data = $(this).data(),
                step = 30,
                flipbook = $('.sj-book'),
                actualPos = $('#slider').slider('value')*step;
    
            if (typeof(data.scrollX)==='undefined') {
                data.scrollX = actualPos;
                data.scrollPage = flipbook.turn('page');
            }
    
            data.scrollX = Math.min($( "#slider" ).slider('option', 'max')*step,
                Math.max(0, data.scrollX + deltaX));
    
            var actualView = Math.round(data.scrollX/step),
                page = Math.min(flipbook.turn('pages'), Math.max(1, actualView*2 - 2));
    
            if ($.inArray(data.scrollPage, flipbook.turn('view', page))==-1) {
                data.scrollPage = page;
                flipbook.turn('page', page);
            }
    
            if (data.scrollTimer)
                clearInterval(data.scrollTimer);
            
            data.scrollTimer = setTimeout(function(){
                data.scrollX = undefined;
                data.scrollPage = undefined;
                data.scrollTimer = undefined;
            }, 1000);
    
        });
    
        // Slider
    
        $( "#slider" ).slider({
            min: 1,
            max: 100,
    
            start: function(event, ui) {
    
                if (!window._thumbPreview) {
                    _thumbPreview = $('<div />', {'class': 'thumbnail'}).html('<div></div>');
                    setPreview(ui.value);
                    _thumbPreview.appendTo($(ui.handle));
                } else
                    setPreview(ui.value);
    
                moveBar(false);
    
            },
    
            slide: function(event, ui) {
    
                setPreview(ui.value);
    
            },
    
            stop: function() {
    
                if (window._thumbPreview)
                    _thumbPreview.removeClass('show');
                
                $('.sj-book').turn('page', Math.max(1, $(this).slider('value')*2 - 2));
    
            }
        });
    
    
        // URIs
        
        Hash.on('^page\/([0-9]*)$', {
            yep: function(path, parts) {
    
                var page = parts[1];
    
                if (page!==undefined) {
                    if ($('.sj-book').turn('is'))
                        $('.sj-book').turn('page', page);
                }
    
            },
            nop: function(path) {
    
                if ($('.sj-book').turn('is'))
                    $('.sj-book').turn('page', 1);
            }
        });
    
        // Arrows
    
        $(document).keydown(function(e){
    
            var previous = 37, next = 39;
    
            switch (e.keyCode) {
                case previous:
    
                    $('.sj-book').turn('previous');
    
                break;
                case next:
                    
                    $('.sj-book').turn('next');
    
                break;
            }
    
        });
    
    
        // Flipbook
    
        flipbook.bind(($.isTouch) ? 'touchend' : 'click', zoomHandle);
    
        flipbook.turn({
            elevation: 50,
            acceleration: !isChrome(),
            autoCenter: true,
            gradients: true,
            duration: 1000,
            pages: 30,
            when: {
                turning: function(e, page, view) {
                    
                    var book = $(this),
                        currentPage = book.turn('page'),
                        pages = book.turn('pages');
    
                    if (currentPage>3 && currentPage<pages-3) {
                    
                        if (page==1) {
                            book.turn('page', 2).turn('stop').turn('page', page);
                            e.preventDefault();
                            return;
                        } else if (page==pages) {
                            book.turn('page', pages-1).turn('stop').turn('page', page);
                            e.preventDefault();
                            return;
                        }
                    } else if (page>3 && page<pages-3) {
                        if (currentPage==1) {
                            book.turn('page', 2).turn('stop').turn('page', page);
                            e.preventDefault();
                            return;
                        } else if (currentPage==pages) {
                            book.turn('page', pages-1).turn('stop').turn('page', page);
                            e.preventDefault();
                            return;
                        }
                    }
    
                    updateDepth(book, page);
                    
                    if (page>=2)
                        $('.sj-book .p2').addClass('fixed');
                    else
                        $('.sj-book .p2').removeClass('fixed');
    
                    if (page<book.turn('pages'))
                        $('.sj-book .p29').addClass('fixed');
                    else
                        $('.sj-book .p29').removeClass('fixed');
    
                    Hash.go('page/'+page).update();
                        
                },
    
                turned: function(e, page, view) {
    
                    var book = $(this);
    
                    if (page==2 || page==3) {
                        book.turn('peel', 'br');
                    }
    
                    updateDepth(book);
                    
                    $('#slider').slider('value', getViewNumber(book, page));
    
                    book.turn('center');
    
                },
    
                start: function(e, pageObj) {
            
                    moveBar(true);
    
                },
    
                end: function(e, pageObj) {
                
                    var book = $(this);
    
                    updateDepth(book);
    
                    setTimeout(function() {
                        
                        $('#slider').slider('value', getViewNumber(book));
    
                    }, 1);
    
                    moveBar(false);
    
                },
    
                missing: function (e, pages) {
    
                    for (var i = 0; i < pages.length; i++) {
                        addPage(pages[i], $(this));
                    }
    
                }
            }
        });
    
    
        $('#slider').slider('option', 'max', numberOfViews(flipbook));
    
        flipbook.addClass('animated');
    
        // Show canvas
    
        $('#canvas').css({visibility: ''});
    }
    
    // Hide canvas
    
    $('#canvas').css({visibility: 'hidden'});
    
    // Load turn.js


    yepnope({
        test : Modernizr.csstransforms,
        yep: ["{{ asset('build/assets/js/lib/turn.min.js') }}"],
        nope: [ "{{ asset('build/assets/js/lib/turn.html4.min.js') }}", "{{ asset('build/assets/css/jquery.ui.html4.css') }}","{{ asset('build/assets/css/yearbook-html4.css') }}" ],
        both: [ "{{ asset('build/assets/js/yearbook.js') }}","{{ asset('build/assets/css/jquery.ui.css') }}", "{{ asset('build/assets/css/yearbook.css') }}"],
        complete: loadApp
    });

// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

    </script>
</body>
</html>