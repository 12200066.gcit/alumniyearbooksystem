<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <x-sidebar/>
  
   
    <!-- <h2 style="text-align: center;">Add Student</h2> -->
    <form method="POST" action="{{route('studentcard.add')}}" enctype="multipart/form-data" class="row">
    {{-- for security purpose use this --}}
    <div>
        @if($errors->any())
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{$error}}</li>    
            @endforeach
        </ul>
        @endif
    </div>
    @csrf
        @method('post')
        <h3 style="position: fixed; top:30px;">Add Student Card</h3>
        <div class="column">
            <div class="inside">
            <div class="floating-label-group">
            <input list='yeardown'  name='year' class="form-control" autocomplete="off" autofocus required />
				<label class="floating-label">Select the graduate Year</label>
                <datalist id="yeardown">
                <option value="">
                </datalist>
			</div>

            <div class="floating-label-group">
			<input type="text"  name="studentid"  class="form-control" autocomplete="off" autofocus required />
            <label class="floating-label">Student Id </label>
			</div>
        
            <div class="floating-label-group">
			<input list='course' name="course"  class="form-control" autocomplete="off" autofocus required />
				<label class="floating-label">Course </label>
                <datalist id="course">
                <option value="Bsc.ITA">
                <option value="Bsc.ITB">
                <option value="Bsc.CS">
                </datalist>
			</div>
            <div class="floating-label-group">
                <textarea name="quote" class="form-control" autocomplete="off" autofocus required></textarea>
			<!-- <input type="textarea" name="quote" class="form-control" autocomplete="off" autofocus required /> -->
				<label class="floating-label">Quote </label>
			</div>

            </div>
       
        </div>
        <div class="column">
            <div class="outside">
            <div class="floating-label-group">
			<input type="text" name="studentname" class="form-control" autocomplete="off" autofocus required />
				<label class="floating-label">Student Name</label>
			</div>
            <label class="picture" for="picture__input" tabIndex="0">
                <span class="picture__image"></span>
              </label>
              <input type="file" name="image" id="picture__input">
            <br><br>
            <input type="submit" value="Add Student" class="form-control">
        </div>
        
        </div>

</form>




    <script>
        // Get the current year
        const currentYear = new Date().getFullYear();
        
        // Get the dropdown element
        const yearDropdown = document.getElementById("yeardown");

        // Create options starting from the current year and incrementing for a specified number of years
        const numberOfYears = 10; // You can change this to the desired number of years
        for (let i = 0; i < numberOfYears; i++) {
            const year = currentYear + i;
            const option = document.createElement("option");
            option.text = year;
            option.value = year;
            yearDropdown.appendChild(option);
        }
        const inputFile = document.querySelector("#picture__input");
        const pictureImage = document.querySelector(".picture__image");
        const pictureImageTxt = "Choose an image";  
        pictureImage.innerHTML = pictureImageTxt;

        inputFile.addEventListener("change", function (e) {
        const inputTarget = e.target;
        const file = inputTarget.files[0];

        if (file) {
        const reader = new FileReader();

        reader.addEventListener("load", function (e) {
        const readerTarget = e.target;

        const img = document.createElement("img");
        img.src = readerTarget.result;
        img.classList.add("picture__img");

        pictureImage.innerHTML = "";
        pictureImage.appendChild(img);
    });

    reader.readAsDataURL(file);
  } else {
    pictureImage.innerHTML = pictureImageTxt;
  }
});
    </script>
</body>
</html>