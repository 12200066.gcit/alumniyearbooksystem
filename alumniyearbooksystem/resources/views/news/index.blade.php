<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <title>Document</title>
</head>
<style>
    main.table {
    margin-top: 10%;
    margin-left: 25%;
    width: 70%;
    height: 90%;
    background-color: #fff5;

    backdrop-filter: blur(7px);
    box-shadow: 0 .4rem .8rem #0005;
    border-radius: .8rem;

    overflow: hidden;
}

.table__header {
    width: 100%;
    height: 10%;
    background-color: #fff4;
    padding: .8rem 1rem;
    display: flex;
    justify-content: space-between;
    align-items: center;
}

.table__header .input-group {
    width: 35%;
    height: 100%;
    background-color: #fff5;
    padding: 0 .8rem;
    border-radius: 2rem;
    display: flex;
    justify-content: center;
    align-items: center;

    transition: .2s;
}

.table__header .input-group:hover {
    width: 45%;
    background-color: #fff8;
    box-shadow: 0 .1rem .4rem #0002;
}

.table__header .input-group img {
    width: 1.2rem;
    height: 1.2rem;
}

.table__header .input-group input {
    width: 100%;
    padding: 0 .5rem 0 .3rem;
    background-color: transparent;
    border: none;
    outline: none;
}

.table__body {
    width: 95%;
    max-height: calc(89% - 1.6rem);
    background-color: #fffb;

    margin: .8rem auto;
    border-radius: .6rem;

    overflow: auto;
    overflow: overlay;
}

.table__body::-webkit-scrollbar{
    width: 0.5rem;
    height: 0.5rem;
}

.table__body::-webkit-scrollbar-thumb{
    border-radius: .5rem;
    background-color: #0004;
    visibility: hidden;
}

.table__body:hover::-webkit-scrollbar-thumb{ 
    visibility: visible;
}

table {
    width: 100%;
}

td img {
    width: 36px;
    height: 36px;
    margin-right: .5rem;
    border-radius: 50%;

    vertical-align: middle;
}

table, th, td {
    border-collapse: collapse;
    padding: 1rem;
    text-align: left;
}

thead th {
    position: sticky;
    top: 0;
    left: 0;
    background-color: #d5d1defe;
    cursor: pointer;
    text-transform: capitalize;
}

tbody tr:nth-child(even) {
    background-color: #0000000b;
}

tbody tr {
    --delay: .1s;
    transition: .5s ease-in-out var(--delay), background-color 0s;
}

tbody tr.hide {
    opacity: 0;
    transform: translateX(100%);
}

tbody tr:hover {
    background-color: #fff6 !important;
}

tbody tr td,
tbody tr td p,
tbody tr td img {
    transition: .2s ease-in-out;
}

tbody tr.hide td,
tbody tr.hide td p {
    padding: 0;
    font: 0 / 0 sans-serif;
    transition: .2s ease-in-out .5s;
}

tbody tr.hide td img {
    width: 0;
    height: 0;
    transition: .2s ease-in-out .5s;
}

.status {
    padding: .4rem 0;
    border-radius: 2rem;
    text-align: center;
}

.status.delivered {
    background-color: #86e49d;
    color: #006b21;
}

.status.cancelled {
    background-color: #d893a3;
    color: #b30021;
}

.status.pending {
    background-color: #ebc474;
}

.status.shipped {
    background-color: #6fcaea;
}


@media (max-width: 1000px) {
    td:not(:first-of-type) {
        min-width: 12.1rem;
    }
}

thead th span.icon-arrow {
    display: inline-block;
    width: 1.3rem;
    height: 1.3rem;
    border-radius: 50%;
    border: 1.4px solid transparent;
    
    text-align: center;
    font-size: 1rem;
    
    margin-left: .5rem;
    transition: .2s ease-in-out;
}

thead th:hover span.icon-arrow{
    border: 1.4px solid #6c00bd;
}

thead th:hover {
    color: #6c00bd;
}

thead th.active span.icon-arrow{
    background-color: #6c00bd;
    color: #fff;
}

thead th.asc span.icon-arrow{
    transform: rotate(180deg);
}

thead th.active,tbody td.active {
    color: #6c00bd;
}
.icon-button{
    border: none;
    background-color: transparent;
}
</style>
<body>
<x-sidebar/>
<main class="table">
<div>
        @if (session()->has('success'))
            <div>
                {{session('success')}}
            </div>
        @endif
    </div>
        <section class="table__header">
            <h1>News Details </h1>
            <form action="/news"  method="get" id="searchForm" class="input-group">
            <i class="fa fa-magnifying-glass" style="position:fixed; right:20px"></i>
                <input type="text" placeholder="Search Data..." name="search" id="search" class="form-control" value="{{ $search }}">
            </form>
            </div>
      
    </div>

        </section>
        <section class="table__body">
            <table>
                <thead>
                    <tr>
                        <th> Year</th>
                        <th> News Title </th>
                        <th> News Content</th>
                        <th> Action </th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($news as $News)
                    <tr>
                        <td> {{$News->year}} </td>
                        <td> <img  src="{{asset('/storage/images/studentcard/'.$News->image)}}" />{{$News->newstitle}}</td>
                        <td>{{{$News->content}}}</td>
                        <td style="display: flex; flex-direction:row;">
                          <a  href="{{route('news.edit',['news'=> $News])}}"><i class="fa-solid fa-pen" style="color: #b35927; margin-right:20px"></i></a>
                          <form method="POST"  action="{{route('news.destroy',['news'=>$News])}}">
                            @csrf
                            @method('delete')
                            <button type="submit" class="icon-button" >
                            <i class="fa-solid fa-trash" style="color: #51311f;"></i>
                            </button>
                          </form>
                        </td>
                    </tr>
                    @endforeach    
                </tbody>
            </table>
            {{$news->links()}}
   
        </section>
    </main>
</body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        let typingTimer; // Timer identifier
        const doneTypingInterval = 500; // Time in milliseconds, adjust as needed

        $('#search').on('input', function() {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(performSearch, doneTypingInterval);
        });

        // When the user clicks outside the input or changes focus
        $('#search').on('blur', function() {
            performSearch(); // Perform search when the user clicks outside the input field
        });

        // Function to perform the search
        function performSearch() {
            $('#searchForm').submit(); // Submit the form to trigger the search
        }
    });
</script>
</html>