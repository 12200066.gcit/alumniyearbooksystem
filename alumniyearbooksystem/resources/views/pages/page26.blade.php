<div class="book-content">
  <div id="index-gallery">
    <h3 class="leadertext">Leaders</h3>
    @foreach($leaders as $leader)
    <div class="item">
      <img src="{{asset('/storage/images/studentcard/'.$leader->image)}}" class="image--cover" />
      <p class="name">{{$leader->name}}</p>
      <p class="position">{{$leader->position}}</p>
    </div>
    @endforeach
  </div>
</div>
<span class="page-number">26</span>
