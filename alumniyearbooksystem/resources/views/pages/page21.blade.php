<style>
  .cont {
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  background-image: linear-gradient(to left bottom, #F2E3C6 0%, #A7A1A5 100%);
  overflow: hidden;
}

.app {
  position: relative;
  min-width:400px;
  height: 100%;
  box-shadow: 0 0 60px rgba(0, 0, 0, 0.3);
  overflow: hidden;
}

.app__bgimg {
  position: absolute;
  top: 0;
  left: -2.5%;
  width: 105%;
  height: 100%;
  transition: transform 3.5s 770ms;
}

.app__bgimg-image {
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
}


.app__bgimg-image--1 {
  background: url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIaYRMIA7NsRNyaLUNIkPHwO8E0ixj2UlKUixQI8I71vT1OH5x9JWNLr7FW4rSozEWF7Y&usqp=CAU');
}

.app__bgimg-image--2 {
  background: url('https://i0.wp.com/blog.inevent.com/wp-content/uploads/2021/05/Blog_InEvent_s_Online_Events_Management_Checklist.png?fit=1180%2C570&ssl=1');
  opacity: 0;
  transition: opacity 0ms 1300ms;
  will-change: opacity;
}

.app__text {
  position: absolute;
  right: 165px;
  top: 150px;
  font-family: 'Roboto', sans-serif;
  text-transform: uppercase;
  z-index: 1;
}

.app__text-line {
  transition: transform 1500ms 400ms, opacity 750ms 500ms;
  will-change: transform, opacity;
  user-select: none;
}

.app__text-line--4 {
  font-size: 50px;
  font-weight: 700;
  color: #0A101D;
}

.app__text-line--3 {
  font-size: 40px;
  font-weight: 300;
}

.app__text-line--2 {
  margin-top: 10px;
  font-size: 14px;
  font-weight: 500;
  color: #0099CC;
}

.app__text-line--1 {
  margin-top: 15px;
}

.app__text-line--1 img {
  width: 50px;
}

.app__text--1 .app__text-line {
  transform: translate3d(0, -125px, 0);
  opacity: 0;
}

.app__text--2 {
  right: initial;
  top: 250px;
  left: 80px;
  z-index: -1;
  transition: z-index 1500ms;
}

.app__text--2 .app__text-line {
  opacity: 0;
  transition: transform 1500ms 300ms, opacity 400ms 500ms;
}

.app__img {
  position: absolute;
  transform: translate3d(0, -750px, 0);
  width: 850px;
  height: 100%;
  transition: transform 3s cubic-bezier(0.6, 0.13, 0.31, 1.02);
  will-change: transform;
}

.initial .app__img {
  transform: translate3d(0, 0, 0);
}

.active .app__bgimg {
  transform: translate3d(10px, 0, 0) scale(1.05);
  transition: transform 5s 850ms ease-in-out;
}

.active .app__bgimg .app__bgimg-image--2 {
  opacity: 1;
  transition: opacity 0ms 1500ms;
}

.active .app__img {
  transition: transform 3s cubic-bezier(0.6, 0.13, 0.31, 1.02);
  transform: translate3d(0, -1410px, 0);
}

.active .app__text--1 {
  z-index: -1;
  transition: z-index 0ms 1500ms;
}

.active .app__text--1 .app__text-line {
  transform: translate3d(0, -125px, 0);
  transition: transform 1500ms 300ms, opacity 400ms 500ms;
  opacity: 0;
}

.active .app__text--2 {
  z-index: 1;
}

.active .app__text--2 .app__text-line {
  transform: translate3d(0, -125px, 0);
  transition: transform 2500ms 1100ms, opacity 1300ms 1300ms;
  opacity: 1;
}

.mouse {
  position: relative;
  margin-right: 20px;
  min-width: 50px;
  height: 80px;
  border-radius: 30px;
  border: 5px solid rgba(255, 255, 255, .8);
}

.mouse:after {
  content: '';
  position: absolute;
  top: 20px;
  left: 50%;
  transform: translate(-50%, 0);
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background-color: #fff;
  animation: scroll 1s infinite alternate;
}

@keyframes scroll {
  100% {
    transform: translate(-50%, 15px);
  }
}

.pages {
  margin-left: 20px;
}

.pages__list {
  list-style-type: none;
}

.pages__item {
  position: relative;
  margin-bottom: 10px;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  border: 3px solid #fff;
  cursor: pointer;
}

.pages__item:after {
  content: '';
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%) scale(0, 0);
  width: 75%;
  height: 75%;
  border-radius: 50%;
  background-color: #fff;
  opacity: 0;
  transition: 500ms;
}

.pages__item:hover:after {
  transform: translate(-50%, -50%) scale(1, 1);
  opacity: 1;
}

.page__item-active:after {
  transform: translate(-50%, -50%) scale(1, 1);
  opacity: 1;
}

.icon-link {
  position: absolute;
  left: 5px;
  bottom: 5px;
  width: 50px;
}

.icon-link img {
  width: 100%;
  vertical-align: top;
}

.icon-link--twitter {
  left: auto;
}
.app__text--1 {
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-start;
  height: 100%;
  width: 100%;
}

.app__text--1 .app__text-line {
  transition: transform 1500ms 400ms, opacity 750ms 500ms;
  will-change: transform, opacity;
  user-select: none;
  opacity: 1;
  transform: translate3d(0, 0, 0);
}


.app__text--1 .app__text-line--4 {
  font-size: 50px;
  font-weight: 700;
  color: #0A101D;
  margin-left:100px
}

.app__text--1 .app__text-line--3 {
  font-size: 40px;
  font-weight: 300;
}

.app__text--1 .app__text-line--2 {
  margin-top: 10px;
  font-size: 14px;
  font-weight: 500;
  color: #0099CC;
}

.app__text--1 .app__text-line--1 {
  margin-top: 15px;
}

.app__text--1 .app__text-line--1 img {
  width: 50px;
}
</style>
<div class="book-content">
<div class="cont">
  <div class="mouse"></div>
  <div class="app">
    <div class="app__bgimg">
      <div class="app__bgimg-image app__bgimg-image--1">
      </div>
      <div class="app__bgimg-image app__bgimg-image--2">
      </div>
    </div>
    <div class="app__img">
      <img onmousedown="return false" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/537051/whiteTest4.png" alt="city" />
    </div>
    
    <div class="app__text app__text--1">
    <div class="app__text-line app__text-line--4">News</div>
    </div>
    
    <div class="app__text app__text--2">
      <div class="app__text-line app__text-line--4">Events</div>
    </div>
  </div>
  <div class="pages">
    <ul class='pages__list'>
      <li data-target='1' class='pages__item pages__item--1 page__item-active'></li>
      <li data-target='2' class='pages__item pages__item--2'></li>
    </ul>
  </div>
</div>
</div>
<span class="page-number">21</span>
<script>
  $(document).ready(function() {
  const $app = $('.app');
  const $img = $('.app__img');
  const $pageNav1 = $('.pages__item--1');
  const $pageNav2 = $('.pages__item--2');
  let animation = true;
  let curSlide = 1;
  let scrolledUp, nextSlide;
  
  let pagination = function(slide, target) {
    animation = true;
    if (target === undefined) {
      nextSlide = scrolledUp ? slide - 1 : slide + 1;
    } else {
      nextSlide = target;
    }
    
    $('.pages__item--' + nextSlide).addClass('page__item-active');
    $('.pages__item--' + slide).removeClass('page__item-active');
    
    $app.toggleClass('active');
    setTimeout(function() {
      animation = false;
    }, 3000)
  }
  
  let navigateDown = function() {
    if (curSlide > 1) return;
    scrolledUp = false;
    pagination(curSlide);
    curSlide++;
  }

  let navigateUp = function() {
    if (curSlide === 1) return;
    scrolledUp = true;
    pagination(curSlide);
    curSlide--;
  }

  setTimeout(function() {
    $app.addClass('initial');
  }, 1500);

  setTimeout(function() {
    animation = false;
  }, 4500);
  
  $(document).on('mousewheel DOMMouseScroll', function(e) {
    var delta = e.originalEvent.wheelDelta;
    if (animation) return;
    if (delta > 0 || e.originalEvent.detail < 0) {
      navigateUp();
    } else {
      navigateDown();
    }
  });

  $(document).on("click", ".pages__item:not(.page__item-active)", function() {
    if (animation) return;
    let target = +$(this).attr('data-target');
    pagination(curSlide, target);
    curSlide = target;
  });
});
</script>