<style>
    @import url('https://fonts.googleapis.com/css2?family=Croissant+One&family=Outfit:wght@100&display=swap');
/* Variables */
:root {
  --dark: #333;
  --yellow: yellowgreen;
  --dark-gray: #58595b;
  --light-gray: #f5f5f5;
  --light-blue: #287cb7;
}

/* Staff Members */
.staff-member {
  display: flex;
}

.staff-member--picture {
  transform: translate(90px, -11px);
  margin-top: 15px;
  height: 100px;
  border-radius: 50%;
  border: #497153 5px solid;
}

.staff-member--card {
  width: 160px;
  height: 100px;
  max-width: 350px;
  padding-top: 15px;
  border-radius: 4px;
  padding-right: 30px;
  padding-left: 100px;
  border: solid 1px #497153;
  background-color: var(--light-gray);
}

.staff-member--name-occupation i {
  /* white-space: nowrap; */
  font-size: 14px;
}

.staff-member--name-occupation {
  line-height: 20px;
  font-size: 18px;
  color: var(--dark-gray);
}

.staff-member--name-occupation:before {
  content: "";
  width: 70px;
  margin-top: 60px;
  position: absolute;
  display: inline-block;
  border-bottom: #497153 3px solid;
}

h3{
    font-family: 'Croissant One', serif;
    font-family: 'Outfit', sans-serif;
    text-align: center;
    color: #497153;
}
</style>

<div class="book-content">
    <h3 class="achievementtext">Achievements</h3>
    @foreach ($achievements as $achievement)
    <div class="staff-member">
      <img src="{{asset('/storage/images/studentcard/'.$achievement->image)}}" class="staff-member--picture img-responsive"/>
      <div class="staff-member--card">
        <div class="staff-member--name-occupation">
          {{$achievement->title}}
          <br>
          <i>{{$achievement->description}}</i>
        </div>
      </div>
    </div>
    @endforeach
</div>  


</div>
<span class="page-number">25</span>