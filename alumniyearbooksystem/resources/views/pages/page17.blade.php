<div class="book-content">
<div class="container">
    <h4 class="heading">Bsc Information Technology B Students</h4>
  <div class="card-content"> 
  
      
  @foreach ($studentcards as $studentcard)
    <div class="card">
      <div class="card-image">
      <img src="{{asset('/storage/images/studentcard/'.$studentcard->image)}}"></img>
      </div>
      <div class="card-info">
      <p class="text">{{$studentcard->id}}</p>
        <p class="text">{{$studentcard->studentname}}</p>
        <p class="text">{{$studentcard->course}}</p>
        <p class="text">{{$studentcard->quote}}</p>
      </div>
    </div>
    @endforeach
  </div>
</div>
</div>
<span class="page-number">17</span>