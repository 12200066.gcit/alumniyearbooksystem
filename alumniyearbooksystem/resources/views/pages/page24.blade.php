
<div class="book-content">
    <h3 class="achievementtext">Achievements</h3>
    @foreach ($achievements as $achievement)
    <div class="staff-member">
      <img src="{{asset('/storage/images/studentcard/'.$achievement->image)}}" class="staff-member--picture img-responsive"/>
      <div class="staff-member--card">
        <div class="staff-member--name-occupation">
          {{$achievement->title}}
          <br>
          <i>{{$achievement->description}}</i>
        </div>
      </div>
    </div>
    @endforeach
</div>  


</div>
<span class="page-number">24</span>