<div class="book-content">
<div class="container">
    <h4 class="heading">Bsc Computer Science Students</h4>
  <div class="card-content"> 
  
      
  @foreach ($studentcards as $studentcard)
    <div class="card">
      <div class="card-image">
      @if(Route::currentRouteName() === 'generate-pdf')
                <img src="{{ public_path('/storage/images/studentcard/' . $studentcard->image) }}" />
            @else
                <img src="{{ asset('storage/images/studentcard/' . $studentcard->image) }}" />
            @endif
      </div>
      <div class="card-info">
      <p class="text">{{$studentcard->id}}</p>
        <p class="text">{{$studentcard->studentname}}</p>
        <p class="text">{{$studentcard->course}}</p>
        <p class="text">{{$studentcard->quote}}</p>
      </div>
    </div>
    @endforeach

  </div>


</div>




<span class="page-number">8</span>
