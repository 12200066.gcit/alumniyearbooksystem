
<style>
  .E-row {
    display: flex;
    height: 500px;
    width: 35%;
    padding: 35px;
    border: 1px solid rgba(255, 255, 255, .25);
    border-radius: 20px;
    background-color: rgba(255, 255, 255, 0.45);
    box-shadow: 0 0 10px 1px rgba(0, 0, 0, 0.25);
    margin-left: 40%;
    backdrop-filter: blur(15px);
    align-items: center;
    justify-content: center;
    margin-top: 10%;
  } 
.f-label-group{
    position: relative;
    /* top:30px; */
    margin-top: 20px;
    margin-bottom: 25px;
    margin-left: 20px;
}
.f-label {
    font-size: 14px;
    color: #cccccc;
    position: absolute;
    pointer-events: none;
    top: 10px;
    left: 12px;
    transition: all 0.1s ease;
    margin-left: 20px;
  }
.f-label-group input:focus ~ .f-label,
.f-label-group input:not(:focus):valid ~ .f-label,
.f-label-group textarea:focus ~.f-label,
.f-label-group textarea:not(:focus):valid ~ .f-label{
    top: -20px;
    font-size: 14px;
    opacity: 1;
    color: #404040;
  }
  .f-control {
              width: 60%;
              padding: 10px;
              /* margin-bottom: -10px; */
              box-sizing: border-box;
              border: 1px solid #ccc;
              border-radius: 3px;
              margin-left: 20px;
          }
        .E-outside{
        padding-left: 20%;
 } 
 .E-picture {
    width: 200px;
    aspect-ratio: 16/9;
    background: #ddd;
    margin-left: 10%;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #aaa;
    /* margin-top: 20%; */
    border: 2px dashed currentcolor;
    cursor: pointer;
    font-family: sans-serif;
    transition: color 300ms ease-in-out, background 300ms ease-in-out;
    outline: none;
    overflow: hidden;
  } 
  
 .E-picture:hover {
    color: #777;
    background: #ccc;
  }
  
  .E-picture:active {
    border-color: turquoise;
    color: turquoise;
    background: #eee;
  }
  
  .E-picture:focus {
    color: #777;
    background: #ccc;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
  } 
    
</style>

<body>
    <x-sidebar/>

    {{-- To catch the error --}}
 
    <form method="POST" action="{{route('leaders.add')}}" enctype="multipart/form-data" class="E-row">
    {{-- for security purpose use this --}}
    <div>
        @if($errors->any())
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{$error}}</li>    
            @endforeach
        </ul>
        @endif
    </div>
    @csrf
        @method('post')
        <h3 style="position: fixed; top:20px;">Add Leaders</h3>
        <div class="column">
            <div class="E-outside">
            <div class="f-label-group">
            <input list='yeardown'  name='year' class="f-control" autocomplete="off" autofocus required />
				<label class="f-label">Select the elected Year</label>
                <datalist id="yeardown">
                <option value="">
                </datalist>
			</div>
            <div class="f-label-group">
			<input type="text" name="name"  class="f-control" autocomplete="off" autofocus required />
				<label class="f-label">Leader Name</label>
			</div>
            <div class="f-label-group">
			<input type="text" name="position" class="f-control" autocomplete="off" autofocus required />
				<label class="f-label">Position</label>
			</div>
            <label class="E-picture" for="picture__input" tabIndex="0">
                <span class="picture__image"></span>
              </label>
              <input type="file" name="image" id="picture__input">
            <br><br>
            <input type="submit" value="Add Leaders"  class="f-control" >
        </div>
        </div>
</form>


    </div>
    <script>
         // Get the current year
         const currentYear = new Date().getFullYear();
        
        // Get the dropdown element
        const yearDropdown = document.getElementById("yeardown");

        // Create options starting from the current year and incrementing for a specified number of years
        const numberOfYears = 10; // You can change this to the desired number of years
        for (let i = 0; i < numberOfYears; i++) {
            const year = currentYear + i;
            const option = document.createElement("option");
            option.text = year;
            option.value = year;
            yearDropdown.appendChild(option);
        }
        const inputFile = document.querySelector("#picture__input");
        const pictureImage = document.querySelector(".picture__image");
        const pictureImageTxt = "Choose an image";  
        pictureImage.innerHTML = pictureImageTxt;

        inputFile.addEventListener("change", function (e) {
        const inputTarget = e.target;
        const file = inputTarget.files[0];

        if (file) {
        const reader = new FileReader();

        reader.addEventListener("load", function (e) {
        const readerTarget = e.target;

        const img = document.createElement("img");
        img.src = readerTarget.result;
        img.classList.add("picture__img");

        pictureImage.innerHTML = "";
        pictureImage.appendChild(img);
    });

    reader.readAsDataURL(file);
  } else {
    pictureImage.innerHTML = pictureImageTxt;
  }
});
    </script>
</body>
</html>