<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentCardController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\YearbookController;
use App\Http\Controllers\EventsController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\LeadersController;
use App\Http\Controllers\AchievementsController;
use App\Http\Controllers\PdfController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/comingsoon',function (){
    return view('commingsoon');
    
})->name('commingsoon');
// route for the yeabook
Route::get('/yearbook', [YearbookController::class, 'index'])->name('yearbook.index');
Route::get('pages/{page}', [PageController::class, 'show']);

//route to download 
Route::get('/download',[PdfController::class,'generatePDF'])->name('generate-pdf');

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth', 'verified'])->name('dashboard');
Route::get('/dashboard',[StudentCardController::class,'total'])->middleware(['auth', 'verified'])->name('dashboard');
Route::get('/studentcard/chart', [StudentCardController::class, 'studentCardChart']);

Route::middleware('auth')->group(function () {
    //routes for studentcard 
    Route::get('/studentcard', [StudentCardController::class, 'index'])->name('studentcard.index');

// to create routes to add in database 
    Route::get('/studentcard/create', [StudentCardController::class, 'create'])->name('studentcard.create');

//routes for the action method of studentcard 
    Route::post('/studentcard', [StudentCardController::class, 'add'])->name('studentcard.add');

//routes for editing page 
    Route::get('/studentcard/{studentcard}/edit',[StudentCardController::class, 'edit'])->name('studentcard.edit');

//routes for the update method
    Route::put('/studentcard/{studentcard}/update',[StudentCardController::class, 'update'])->name('studentcard.update');

//routes for the delete method
    Route::delete('/studentcard/{studentcard}/destroy', [StudentCardController::class, 'destroy'])->name('studentcard.destroy');

//routes for events 
    Route::get('/events', [EventsController::class, 'index'])->name('events.index');
//to create routes to add in database 
    Route::get('/events/create', [EventsController::class, 'create'])->name('events.create');
//routes for the action method of events
    Route::post('/events', [EventsController::class, 'add'])->name('events.add');
//editing event page
    Route::get('/events/{events}/edit',[EventsController::class, 'edit'])->name('events.edit');
//routes for the update method
    Route::put('/events/{events}/update',[EventsController::class, 'update'])->name('events.update');
//routes for the delete method
    Route::delete('/events/{events}/destroy', [EventsController::class, 'destroy'])->name('events.destroy');

//routes for news
    Route::get('/news', [NewsController::class, 'index'])->name('news.index');
//to create routes to add in database 
    Route::get('/news/create', [NewsController::class, 'create'])->name('news.create');
//routes for the action method of news
    Route::post('/news', [NewsController::class, 'add'])->name('news.add');
//editing event page
    Route::get('/news/{news}/news',[NewsController::class, 'edit'])->name('news.edit');
//routes for the update method
    Route::put('/news/{news}/update',[NewsController::class, 'update'])->name('news.update');
//routes for the delete method
    Route::delete('/news/{news}/destroy', [NewsController::class, 'destroy'])->name('news.destroy');

//Leader routes
    Route::get('/leaders', [LeadersController::class, 'index'])->name('leaders.index');
//to create routes to add in database 
    Route::get('/leaders/create', [LeadersController::class, 'create'])->name('leaders.create');
//routes for the action method of news
    Route::post('/leaders', [LeadersController::class, 'add'])->name('leaders.add');
//editing event page
    Route::get('/leaders/{leaders}/leaders',[LeadersController::class, 'edit'])->name('leaders.edit');
//routes for the update method
    Route::put('/leaders/{leaders}/update',[LeadersController::class, 'update'])->name('leaders.update');
//routes for the delete method
    Route::delete('/leaders/{leaders}/destroy', [LeadersController::class, 'destroy'])->name('leaders.destroy');


//achievement routes
    Route::get('/achievements', [AchievementsController::class, 'index'])->name('achievements.index');
//to create routes to add in database 
    Route::get('/achievements/create', [AchievementsController::class, 'create'])->name('achievements.create');
//routes for the action method of news
    Route::post('/achievements', [AchievementsController::class, 'add'])->name('achievements.add');
//editing event page
    Route::get('/achievements/{achievements}/achievements',[AchievementsController::class, 'edit'])->name('achievements.edit');
//routes for the update method
    Route::put('/achievements/{achievements}/update',[AchievementsController::class, 'update'])->name('achievements.update');
//routes for the delete method
    Route::delete('/achievements/{achievements}/destroy', [AchievementsController::class, 'destroy'])->name('achievements.destroy');

    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
