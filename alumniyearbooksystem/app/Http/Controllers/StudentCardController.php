<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\studentcard;
class StudentCardController extends Controller
{
    //
    public function index(Request $request){
        // $Studentcards= studentcard::all();
        // $Studentcards= studentcard::paginate(5);
        // $query =studentcard::query();
        // if($request->has('search') && !empty($request->input('search'))){
        //     $query->where('studentid','like','%' . $request->input('search') .'%');
        // }
         $query =studentcard::query();
        if ($request->has('search') && !empty($request->input('search'))) {
            $searchTerm = '%' . $request->input('search') . '%';
            $query->where(function($q) use ($searchTerm) {
                $q->where('studentid', 'like', $searchTerm)
                  ->orWhere('studentname', 'like', $searchTerm)
                  ->orWhere('course', 'like', $searchTerm)
                  ->orWhere('year', 'like', $searchTerm);
            });
        }
    
        $Studentcards =$query->paginate(5)->withQueryString();
        return view('studentcard.index',['Studentcards'=> $Studentcards, 'search' => $request->input('search')]);
    }

    public function create(){
        return view('studentcard.create');
    }
    public function add(Request $request){
        // to show the data in frontend 
        // dd($request);
        $request->validate([
            'year'=>'required',
            'studentid'=>'required|unique:studentcard,studentid',
            'studentname'=> 'required',
            'course'=> 'required',
            'quote'=> 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg'
        ]);
        $input =$request ->all();
        //after requesting we need to save the image in the storage
        if ($request-> hasFile('image'))
        {
            //path for the images to be store
            $destination_path ='public/images/studentcard';
            //image for the client 
            $image =$request->file('image');
            $image_name =$image->getClientOriginalName();
            $path  =$request->file('image')->storeAs($destination_path,$image_name);

            $input['image']=$image_name;
        }
        $newstudentcard=studentcard::create($input);

        return redirect(route('studentcard.index'))->with('success','student successfully added');;
    }
    
    public function edit(studentcard $studentcard){
            // dd($studentcard);
            return view('studentcard.edit',['studentcard'=>$studentcard]);
    }
    public function update(studentcard $studentcard ,Request $request){
        $data = $request->validate([
            'year'=>'required',
            'studentid'=>'required|unique:studentcard,studentid',
            'studentname'=> 'required',
            'course'=> 'required',
            'quote'=> 'required',
            // 'image' => 'required',
        ]);
        if ($request-> hasFile('image'))
        {
            //path for the images to be store
            $destination_path ='public/images/studentcard';
               // Delete the old image if it exists
            // if (Storage::exists($destination_path . '/' . $events->image)) {
            //     Storage::delete($destination_path . '/' . $events->image);
            // }
            //image for the client 
            $image =$request->file('image');
            $image_name =$image->getClientOriginalName();
            $path  =$request->file('image')->storeAs($destination_path,$image_name);
    
            $input['image']=$image_name;
        }
        $studentcard->update($data);
        return redirect(route('studentcard.index'))->with('success','student updated successfully');
    }
    public function destroy(studentcard $studentcard) {
        $studentcard->delete();
        return redirect(route('studentcard.index'))->with('success', 'Student deleted successfully');
    }
    public function total(studentcard $studentcard){
        $totalCS = $studentcard::where('course', 'Bsc.CS')->count();
        $totalIT = $studentcard::whereIn('course', ['Bsc.ITA', 'Bsc.ITB'])->count();

        return view('dashboard',['totalCS'=>$totalCS,'totalIT'=>$totalIT]);
    }
    public function studentCardChart()
{
    $courses = StudentCard::distinct('course')->pluck('course');
    $data = [];

    foreach ($courses as $course) {
        $count = studentcard::where('course', $course)->count();
        $data['labels'][] = $course;
        $data['counts'][] = $count;
    }

    return response()->json($data);
}

}
