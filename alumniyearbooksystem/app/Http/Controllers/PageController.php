<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\studentcard;
use App\Models\events;
use App\Models\news;
use App\Models\leaders;
use App\Models\achievements;

class PageController extends Controller
{
    
    public function show($page) {
   
        // $studentcards = studentcard::all();
       
        // // echo $studentcards;
        // return view("pages.$page",['studentcards'=> $studentcards]);
        // echo $page;
        // if ($page>='page21'){
        //     $events =events::all();
        //     return  view("pages.$page",['events'=>$events]);
        // }
        $course = '';
        if ($page >= "page7" || $page <=  "page10") {
            $course = 'Bsc.CS';
        } elseif ($page >= 'page11' && $page <=  'page16') {
            $course = 'Bsc.ITA';
        }
        elseif ($page >= 'page17' && $page <=  'page20') {
            $course = 'Bsc.ITB';
        }
        elseif($page>='page21' && $page <='page23'){
            $events =events::where('year',2023)->get();
            $news =news::where('year',2023)->get();
            return  view("pages.$page",['events'=>$events,'news'=>$news]);
        }
        elseif($page>='page24' && $page<='page25'){
            $achievements = achievements::where('year',2023)->get();
            return  view("pages.$page", ['achievements'=>$achievements]);
            
        }
        else {
            // Handle other cases or throw an error if needed
            // For example, return a 404 not found page.
            $leaders =leaders::where('year',2023)->get();
            return  view("pages.$page",['leaders'=>$leaders]);

        }
     
        // Query the database for the specific course
        $studentcards = studentcard::where('course', $course)
                        ->where('year', 2023)
                        ->get();
        // $studentcards = studentcard::where('course', $course)->paginate(1);
        // echo $studentcards;
        return view("pages.$page", ['studentcards' => $studentcards]);
    }    

}
