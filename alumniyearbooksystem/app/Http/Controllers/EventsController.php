<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\events;
class EventsController extends Controller
{
    //
    public function index(Request $request){
        // $events= events::all();
        $query =events::query();
        if ($request->has('search') && !empty($request->input('search'))) {
            $searchTerm = '%' . $request->input('search') . '%';
            $query->where(function($q) use ($searchTerm) {
                $q->where('year', 'like', $searchTerm)
                  ->orWhere('title', 'like', $searchTerm);
            });
        }
        $events =$query->paginate(5)->withQueryString();
        return view('events.index',['events'=> $events,'search' => $request->input('search')]);
    }
    public function create(){
        return view('events.create');
    }
    public function add(Request $request){
        // to show the data in frontend 
        // dd($request);
        $request->validate([
            'year'=>'required',
            'title'=> 'required',
            'content'=> 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg'
        ]);
        $input =$request ->all();
        //after requesting we need to save the image in the storage
        if ($request-> hasFile('image'))
        {
            //path for the images to be store
            $destination_path ='public/images/studentcard';
          
            //image for the client 
            $image =$request->file('image');
            $image_name =$image->getClientOriginalName();
            $path  =$request->file('image')->storeAs($destination_path,$image_name);

            $input['image']=$image_name;
        }
        $newevents=events::create($input);

        return redirect(route('events.index'))->with('success','events successfully added');;
    }
    public function edit(events $events){
        // dd($events);
        return view('events.edit',['events'=>$events]);
}
    public function update(events $events ,Request $request){
    $request->validate([
        'year'=>'required',
        'title'=> 'required',
        'content'=> 'required',
        // 'image' => 'required'
    ]);
    $input =$request ->all();
    //after requesting we need to save the image in the storage
    if ($request-> hasFile('image'))
    {
        //path for the images to be store
        $destination_path ='public/images/studentcard';
           // Delete the old image if it exists
        // if (Storage::exists($destination_path . '/' . $events->image)) {
        //     Storage::delete($destination_path . '/' . $events->image);
        // }
        //image for the client 
        $image =$request->file('image');
        $image_name =$image->getClientOriginalName();
        $path  =$request->file('image')->storeAs($destination_path,$image_name);

        $input['image']=$image_name;
    }
    $events->update($input);

    return redirect(route('events.index'))->with('success','eventsupdated successfully');
}
public function destroy(events $events) {
    $events->delete();
    return redirect(route('events.index'))->with('success', 'events deleted successfully');
}
}
