<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\studentcard;
use App\Models\events;
use App\Models\news;

class PdfController extends Controller
{

    // public function generatePDF(){
    //     $pdf = Pdf::loadView('yearbook.index');
    //     return $pdf->download('yearbook.pdf');

    // }
    public function generatePDF(){
        $studentcards= studentcard::paginate(1);
        $events= events::paginate(1);
        $news= news::paginate(1);
        $views = [
            'yearbook.index',
            'pages.page5',
            'pages.page7',
            'pages.page21' // Add more views as needed
        ];
        $pdf = PDF::loadView('pdf.merged', compact('views','studentcards','events','news'));

        return $pdf->download('yearbook.pdf');
    }
}

