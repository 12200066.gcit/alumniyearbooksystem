<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\leaders;


class LeadersController extends Controller
{
    //
    public function index(Request $request){
        // $leaders= leaders::all();
        $query =leaders::query();
        if ($request->has('search') && !empty($request->input('search'))) {
            $searchTerm = '%' . $request->input('search') . '%';
            $query->where(function($q) use ($searchTerm) {
                $q->where('year', 'like', $searchTerm)
                  ->orWhere('name', 'like', $searchTerm)
                  ->orWhere('position', 'like', $searchTerm);
            });
        }
    
        $leaders =$query->paginate(5)->withQueryString();
        return view('leaders.index',['leaders'=> $leaders,'search' => $request->input('search')]);
    }
    public function create(){
        return view('leaders.create');
    }
    public function add(Request $request){
        // to show the data in frontend 
        // dd($request);
        $request->validate([
            'year'=>'required',
            'name'=> 'required',
            'position'=> 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg'
        ]);
        $input =$request ->all();
        //after requesting we need to save the image in the storage
        if ($request-> hasFile('image'))
        {
            //path for the images to be store
            $destination_path ='public/images/studentcard';
          
            //image for the client 
            $image =$request->file('image');
            $image_name =$image->getClientOriginalName();
            $path  =$request->file('image')->storeAs($destination_path,$image_name);

            $input['image']=$image_name;
        }
        $newleaders=leaders::create($input);

        return redirect(route('leaders.index'))->with('success','leader successfully added');;
    }
    public function edit(leaders $leaders){
        // dd($events);
        return view('leaders.edit',['leaders'=>$leaders]);
}
    public function update(leaders $leaders ,Request $request){
    $request->validate([
        'year'=>'required',
        'name'=> 'required',
        'position'=> 'required',
        // 'image' => 'required'
    ]);
    $input =$request ->all();
    //after requesting we need to save the image in the storage
    if ($request-> hasFile('image'))
    {
        //path for the images to be store
        $destination_path ='public/images/studentcard';
           // Delete the old image if it exists
        // if (Storage::exists($destination_path . '/' . $events->image)) {
        //     Storage::delete($destination_path . '/' . $events->image);
        // }
        //image for the client 
        $image =$request->file('image');
        $image_name =$image->getClientOriginalName();
        $path  =$request->file('image')->storeAs($destination_path,$image_name);

        $input['image']=$image_name;
    }
    $leaders->update($input);

    return redirect(route('leaders.index'))->with('success','leader updated successfully');
}
public function destroy(leaders $leaders) {
    $leaders->delete();
    return redirect(route('leaders.index'))->with('success', 'leader deleted successfully');
}
}
