<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\news;
class NewsController extends Controller
{
    public function index(Request $request){
        // $news= news::all();
        $query =news::query();
        if ($request->has('search') && !empty($request->input('search'))) {
            $searchTerm = '%' . $request->input('search') . '%';
            $query->where(function($q) use ($searchTerm) {
                $q->where('newstitle', 'like', $searchTerm)
                  ->orWhere('year', 'like', $searchTerm);
            });
        }
        $news=$query->paginate(5)->withQueryString();
        return view('news.index',['news'=> $news,'search' => $request->input('search')]);
    }
    public function create(){
        return view('news.create');
    }
    public function add(Request $request){
        // to show the data in frontend 
        // dd($request);
        $request->validate([
            'year'=>'required',
            'newstitle'=> 'required',
            'content'=> 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg'
        ]);
        $input =$request ->all();
        //after requesting we need to save the image in the storage
        if ($request-> hasFile('image'))
        {
            //path for the images to be store
            $destination_path ='public/images/studentcard';
          
            //image for the client 
            $image =$request->file('image');
            $image_name =$image->getClientOriginalName();
            $path  =$request->file('image')->storeAs($destination_path,$image_name);

            $input['image']=$image_name;
        }
        $newNews=news::create($input);

        return redirect(route('news.index'))->with('success','news successfully added');;
    }
    public function edit(news $news){
        // dd($events);
        return view('news.edit',['news'=>$news]);
}
    public function update(news $news ,Request $request){
    $request->validate([
        'year'=>'required',
        'title'=> 'required',
        'content'=> 'required',
        // 'image' => 'required'
    ]);
    $input =$request ->all();
    //after requesting we need to save the image in the storage
    if ($request-> hasFile('image'))
    {
        //path for the images to be store
        $destination_path ='public/images/studentcard';
           // Delete the old image if it exists
        // if (Storage::exists($destination_path . '/' . $events->image)) {
        //     Storage::delete($destination_path . '/' . $events->image);
        // }
        //image for the client 
        $image =$request->file('image');
        $image_name =$image->getClientOriginalName();
        $path  =$request->file('image')->storeAs($destination_path,$image_name);

        $input['image']=$image_name;
    }
    $news->update($input);

    return redirect(route('news.index'))->with('success','news updated successfully');
}       
    public function destroy(news $news) {
    $news->delete();
    return redirect(route('news.index'))->with('success', 'news deleted successfully');
}
}
