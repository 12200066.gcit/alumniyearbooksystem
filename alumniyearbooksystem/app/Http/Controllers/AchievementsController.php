<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\achievements;
class AchievementsController extends Controller
{
    //
    public function index(Request $request){
        // $achievements= achievements::all();
        $query =achievements::query();
        if ($request->has('search') &&!empty($request->input('search'))){
            $searchterm = '%' .$request->input('search') . '%';
            $query ->where(function($q) use ($searchterm){
                $q ->where('year','like',$searchterm)
                    ->orWhere('title','like',$searchterm);
            });
        }
        $achievements =$query->paginate(5)->withQueryString();
        return view('achievements.index',['achievements'=> $achievements,'search'=>$request->input('search')]);
    }
    public function create(){
        return view('achievements.create');
    }
    public function add(Request $request){
        // to show the data in frontend 
        // dd($request);
        $request->validate([
            'year'=>'required',
            'title'=> 'required',
            'description'=> 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg'
        ]);
        $input =$request ->all();
        //after requesting we need to save the image in the storage
        if ($request-> hasFile('image'))
        {
            //path for the images to be store
            $destination_path ='public/images/studentcard';
          
            //image for the client 
            $image =$request->file('image');
            $image_name =$image->getClientOriginalName();
            $path  =$request->file('image')->storeAs($destination_path,$image_name);

            $input['image']=$image_name;
        }
        $newachievements=achievements::create($input);

        return redirect(route('achievements.index'))->with('success','achievements successfully added');;
    }
    public function edit(achievements $achievements){
        // dd($events);
        return view('achievements.edit',['achievements'=>$achievements]);
}
    public function update(achievements $achievements ,Request $request){
    $request->validate([
        'year'=>'required',
        'title'=> 'required',
        'description'=> 'required',
        // 'image' => 'required'
    ]);
    $input =$request ->all();
    //after requesting we need to save the image in the storage
    if ($request-> hasFile('image'))
    {
        //path for the images to be store
        $destination_path ='public/images/studentcard';
           // Delete the old image if it exists
        // if (Storage::exists($destination_path . '/' . $events->image)) {
        //     Storage::delete($destination_path . '/' . $events->image);
        // }
        //image for the client 
        $image =$request->file('image');
        $image_name =$image->getClientOriginalName();
        $path  =$request->file('image')->storeAs($destination_path,$image_name);

        $input['image']=$image_name;
    }
    $achievements->update($input);

    return redirect(route('achievements.index'))->with('success','achievements updated successfully');
}
public function destroy(achievements $achievements) {
    $achievements->delete();
    return redirect(route('achievements.index'))->with('success', 'achievements deleted successfully');
}
}
