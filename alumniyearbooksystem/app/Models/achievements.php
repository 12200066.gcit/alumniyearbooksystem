<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class achievements extends Model
{
    use HasFactory;
    protected $table = 'achievements';
    //in fillable we are putting the column name as user is insert the data in the this table
    protected $fillable =[
        'id',
        'year',
        'title',
        'description',
        'image'
    ];
    public $timestamps = false;
}
