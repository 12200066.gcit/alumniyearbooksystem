<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class leaders extends Model
{
    use HasFactory;
    protected $table = 'leaders';
    //in fillable we are putting the column name as user is insert the data in the this table
    protected $fillable =[
        'id',
        'year',
        'name',
        'position',
        'image'
    ];
    public $timestamps = false;
}
