<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class events extends Model
{
    use HasFactory;
    protected $table = 'events';
    //in fillable we are putting the column name as user is insert the data in the this table
    protected $fillable =[
        'id',
        'year',
        'title',
        'content',
        'image'
    ];
    public $timestamps = false;
}
