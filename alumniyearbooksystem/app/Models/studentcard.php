<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class studentcard extends Model
{
    use HasFactory;
    protected $table = 'studentcard';
    //in fillable we are putting the column name as user is insert the data in the this table
    protected $fillable =[
        'year',
        'studentid',
        'studentname',
        'course',
        'quote',
        'image'
    ];
    public $timestamps = false;
}
