<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class news extends Model
{
    use HasFactory;
    protected $table = 'news';
    //in fillable we are putting the column name as user is insert the data in the this table
    protected $fillable =[
        'id',
        'year',
        'newstitle',
        'content',
        'image'
    ];
    public $timestamps = false;
}
